#Jeu du "pierre feuille ciseaux"
import random
import time
import pygame
pygame.init()

class var:
    
    run = True
    ordi_play = False
    score_Player = 0
    score_Ia = 0
    message = str()

pygame.display.set_caption("Pierre, Feuille, Ciseaux")
screen = pygame.display.set_mode((500, 300))

#affichage de base
#Pierre
Pierre = pygame.draw.rect(screen, (0, 255, 0), (50, 125, 100, 50))
screen.blit(pygame.font.Font(None, 35).render("Pierre", 1, (0, 0, 0)), (65, 140))
#Feuille
Feuille = pygame.draw.rect(screen, (255, 0, 0), (200, 125, 100, 50))
screen.blit(pygame.font.Font(None, 35).render("Feuille", 1, (0, 0, 0)), (210, 140))
#Ciseaux
Ciseaux = pygame.draw.rect(screen, (0, 0, 255), (350, 125, 100, 50))
screen.blit(pygame.font.Font(None, 35).render("Ciseaux", 1, (0, 0, 0)), (350, 140))

def affichage(screen):
    #score Player
    pygame.draw.rect(screen, (128, 128, 128), (0, 0, 100, 50))
    screen.blit(pygame.font.Font(None, 35).render(str(var.score_Player), 1, (0, 0, 0)), (40, 15))
    #points Ia
    pygame.draw.rect(screen, (128, 128, 128), (400, 0, 100, 50))
    screen.blit(pygame.font.Font(None, 35).render(str(var.score_Ia), 1, (0, 0, 0)), (440, 15))
    #message
    pygame.draw.rect(screen, (255, 255, 255), (125, 0, 250, 50))
    screen.blit(pygame.font.Font(None, 35).render(var.message, 1, (0, 0, 0)), (205, 15))
affichage(screen)

#Ordi
def ordi(screen):
    if var.ordi_play:
        Ia = random.choice(["Pierre", "Feuille", "Ciseaux"])
        screen.blit(pygame.font.Font(None, 80).render(Ia, 1, (0, 0, 0)), (210, 175))
        pygame.display.flip() #refresh
        time.sleep(1.5) #affichage le choix de l'ordi pendant 1.5s
        calculs_scores(Player, Ia)
    pygame.draw.rect(screen, (255, 255, 255), (50, 175, 400, 50))
    screen.blit(pygame.font.Font(None, 80).render("ordi : ", 1, (0, 0, 0)), (60, 175))
    pygame.display.flip() #refresh
ordi(screen)

#résultats
def calculs_scores(Player, Ia):
    #win
    if Ia == "Pierre" and Player == "Feuille" or Ia == "Feuille" and Player == "Ciseaux" or Ia == "Ciseaux" and Player == "Pierre":
        var.message = "Gagné !"
        var.score_Player += 1 # + 1 point pour le joueur
    #lose
    elif Ia == "Pierre" and Player == "Ciseaux" or Ia == "Ciseaux" and Player == "Feuille" or Ia == "Feuille" and Player == "Pierre":
        var.message = "Perdu !"
        var.score_Ia += 1 # + 1 point pour l'Ia
    #equality
    elif Ia == Player:
        var.message = "Egalité !"
    affichage(screen)
    
#events
while var.run:
    for event in pygame.event.get():
        if event.type == pygame.QUIT or event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:
            var.run = False
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if Pierre.collidepoint(event.pos):
                Player = "Pierre"
            elif Feuille.collidepoint(event.pos):
                Player = "Feuille"
            elif Ciseaux.collidepoint(event.pos):
                Player = "Ciseaux"
            var.ordi_play = True
            ordi(screen)